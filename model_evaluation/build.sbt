name := "model_evaluation"

version := "1.0.0"

scalaVersion := "2.12.8"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.5"

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  "main.jar"
}