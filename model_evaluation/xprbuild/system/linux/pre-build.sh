#! /bin/bash
## This script is used to setup the build environment. It is used setup the build and test environment. It performs
## either of these tasks
##   1. Install Linux Libraries
##   2. Setup environment
##   3. Download data required to perform build.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

set -e

# Installing sbt, when not using hseeberger/scala-sbt:8u212_1.2.8_2.12.8
if command -v sbt &> /dev/null; then
    echo "Found sbt installation, not installing"
else
    echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add
    apt-get update && apt-get install -y sbt
fi

# Installing scala, when not using hseeberger/scala-sbt:8u212_1.2.8_2.12.8
if command -v scalac &> /dev/null; then
    echo "Found scala installation, not installing"
else
    wget https://downloads.lightbend.com/scala/2.12.8/scala-2.12.8.deb
    dpkg -i scala-2.12.8.deb
fi
