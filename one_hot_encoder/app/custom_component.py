"""
This is sample to write custom pyspark component
using xpresso.ai' base classes

Define your component a transformer or estimator and remove the other.
This file can be renamed without harm. Make sure your imports reflect the
renames. The class names (CustomComponentTransformer and
CustomComponentEstimator) can also be renamed without harm.
"""

__author__ = "xpresso.ai"

from pyspark.ml.feature import StringIndexer
from pyspark.ml.feature import VectorAssembler

from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineTransformer
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomComponentTransformer(VectorAssembler,
                                AbstractSparkPipelineTransformer):
    """
    This is a sample to create custom transformer
    """

    def __init__(self, name, xpresso_run_name, inputCols=None, outputCol=None):
        # Change this to use the transformer that needs to be extended
        VectorAssembler.__init__(self,
                                 inputCols=inputCols,
                                 outputCol=outputCol)

        # DO NOT CHANGE
        AbstractSparkPipelineTransformer.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name = 'one_hot_encoder' # this must match the name of the component

    # DO NOT CHANGE, use as it is for transformer
    def _transform(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        ds = super()._transform(dataset)
        self.completed()
        self.state = ds
        return ds


class CustomComponentEstimator(StringIndexer, AbstractSparkPipelineEstimator):
    """
    This is a sample to create custom estimator
    """

    def __init__(self, name, xpresso_run_name, inputCol=None, outputCol=None,
                 handleInvalid='skip', stringOrderType='frequencyDesc'):
        # Change this to use the estimator that needs to be extended
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol,
                               handleInvalid=handleInvalid,
                               stringOrderType=stringOrderType)

        # DO NOT CHANGE
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name = 'one_hot_encoder' # this must match the name of the component

    # DO NOT CHANGE, use as it is for estimator
    def _fit(self, dataset):
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        self.completed()
        return model